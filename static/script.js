function sell_choice(car_info) {
    var checkBox = document.getElementById("check");
    var submitInput = document.getElementById("submit");
    var priceInput = document.getElementById("price");

    if (checkBox.checked == true) {
        priceInput.style.display = "block";
        submitInput.value = "SELL";
    } else {
        submitInput.value = "SELL FOR " + String(car_info) + " $"
        priceInput.style.display = "none";
    }
}


function trees() {

  var checkPostfixe = document.getElementById("postfixe");
  var checkInfixe = document.getElementById("infixe");
  var checkPrefixe = document.getElementById("prefixe");

  var postfixe_form = document.getElementById("postfixe_form");
  var infixe_form = document.getElementById("infixe_form");
  var prefixe_form = document.getElementById("prefixe_form");

  var div_prefixe = document.getElementById("prefixe_div");
  var div_infixe = document.getElementById("infixe_div");
  var div_postfixe = document.getElementById("postfixe_div");

  var paragraph = document.getElementById("parcours_para");

  if (checkPostfixe.checked == true) {
    div_postfixe.style.display = "flex";
    paragraph.style.display = "block";
    checkInfixe.style.display = "none";
    checkPrefixe.style.display = "none";
    infixe_form.style.display = "none";
    prefixe_form.style.display = "none";

  } else if (checkInfixe.checked == true) {
    div_infixe.style.display = "flex";
    paragraph.style.display = "block";
    checkPostfixe.style.display = "none";
    checkPrefixe.style.display = "none";
    postfixe_form.style.display = "none";
    prefixe_form.style.display = "none";

  } else if (checkPrefixe.checked == true) {
    div_prefixe.style.display = "flex";
    paragraph.style.display = "block";
    checkInfixe.style.display = "none";
    checkPostfixe.style.display = "none";
    infixe_form.style.display = "none";
    postfixe_form.style.display = "none";

  } else {
    div_postfixe.style.display = "none";
    div_prefixe.style.display = "none";
    div_infixe.style.display = "none";
    paragraph.style.display = "none";
    infixe_form.style.display = "flex";
    postfixe_form.style.display = "flex";
    prefixe_form.style.display = "flex";
    checkInfixe.style.display = "flex";
    checkPrefixe.style.display = "flex";
    checkPostfixe.style.display = "flex";

  }
}
