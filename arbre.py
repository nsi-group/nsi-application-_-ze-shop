#### ARBRE ####


# IMPORTS #
from anytree.exporter import DotExporter
####

class Arbre:
    """
    permet de creer un arbre binaire
    """
    ## Constructeur
    def __init__(self, data):
        self.data = data # valeur du noeud
        self.left = None # noeud a gauche
        self.right = None # noeud a droite

    #### getters et setters ####
    def setData(self, data):
        self.data = data

    def setLeft(self, arbre):
        self.left = arbre

    def setRight(self, arbre):
        self.right = arbre

    def getLeft(self):
        return self.left

    def getRight(self):
        return self.right

    def getData(self):
        return self.data
    #############



###### FONCTIONS ######

def prefixe(tree, liste) -> list:
    """
    parcours d'arbre prefixe
    renvoie la liste du parcours
    :tree: objet Arbre
    :liste: liste finale
    """
    if tree is None :
        return 0
    liste += [tree.getData()]
    prefixe(tree.getLeft(), liste)
    prefixe(tree.getRight(), liste)
    return liste



def infixe(tree, liste) -> list:
    """
    parcours d'arbre infixe
    renvoie la liste du parcours
    :tree: objet Arbre
    :liste: liste finale
    """
    if tree is None :
        return 0
    infixe(tree.getLeft(), liste)
    liste += [tree.getData()]
    infixe(tree.getRight(), liste)
    return liste



def postfixe(tree, liste) -> list:
    """
    parcours d'arbre postfixe
    renvoie la liste du parcours
    :tree: objet Arbre
    :liste: liste finale
    """
    if tree is None :
        return 0
    postfixe(tree.getLeft(), liste)
    postfixe(tree.getRight(), liste)
    liste += [tree.getData()]
    return liste


def exportToPicture(tree):
    """
    permet de creer la photo tree.png
    """
    DotExporter(tree).to_picture("static/tree.png")
