#### MAIN ####

# IMPORTS #
from flask import Flask, render_template, request, url_for, redirect, abort, session, flash
from flask_session import Session
from ast import literal_eval
from datetime import datetime
from math import ceil
from anytree import *

import arbre as arbre
import manage_db as mdb
import os

###########


## INITIALISATION ##

app = Flask(__name__)
sess = Session()

####################


############
## ROUTES ##
############


@app.route("/") # route vers la page d'accueil
def home():
    """
    permet l'affichage adaptatif de la page d'accueil
    """
    session["percent_drop"] = 0.90 # pourcentage de baisse pour le shop

    if "email" in session: # vérification récurrente sur l'authentification
        return render_template("home.html", 
                                connected = True, 
                                email = session["email"], 
                                name = session["name"])
    return render_template("home.html", connected = False)




@app.route("/signup", methods=["POST", "GET"]) # route vers la page d'inscription
def signup():
    """
    permet d'inscrire un nouvel utilisateur
    """
    if request.method == "POST": # permet l'inscription de l'utilisateur
        dicti = request.form
        date = str(tuple(datetime.now().timetuple())[0:3])
        addition = mdb.addUser(dicti, date)
        if addition:
            return redirect(url_for('login')) # redirige vers la page login si l'utilisateur n'est pas déjà inscrit
        else:
            return render_template("signup.html", result = True) # recommence la procedure

    # affichage de la page de base
    return render_template("signup.html", result = False)


@app.route("/login", methods=["POST", "GET"]) # route vers la page de connexion
def login():
    """
    permet de connecter un utilisateur
    """
    if request.method == "POST":
        dicti = request.form
        user_data = mdb.authUser(dicti) # la fonction renvoie true ou false
        

        if user_data: # si l'utilisateur est bien connecte

            # creation cookies d'utilisateur
            session["id"] = user_data[0]
            session["email"] = user_data[1]
            session["name"] = user_data[2]
            session["new"] = user_data[3]
            session["money"] = user_data[4]

            age = literal_eval(user_data[5])
            diff = datetime.now() - datetime(*age)
            session["age"] = diff.days
            ###

            # verifiation de l'age du compte
            if session["new"] == 0:
                date = literal_eval(user_data[6])
                session["date"] = datetime(*date).__str__().split()
            else:
                session["date"] = ([], [])

            return redirect(url_for('home')) # renvoi vers la page home en cas de succes
        return render_template("login.html", err = True) # renvoi de la page dans le cas d'une erreur
    return render_template("login.html", err = False) # renvoi par defaut de la page de connexion


@app.route("/logout") # route vers home
def logout():
    """
    permet de deconnecter un utilisateur
    """
    # mise a jour date de dernière connexion 
    date = str(tuple(datetime.now().timetuple())[0:6])
    mdb.addDate(session["id"], date)
    # suppression cookies
    session.clear()
    return redirect(url_for('home')) # renvoi vers la page home



@app.route("/profile", methods=["POST", "GET"]) # route vers la page profile de l'utilisateur
def profile():
    """
    permet d'afficher et de gerer le profil d'un utilisateur
    """
    if "email" not in session:
        return redirect(url_for('home'))

    # recuperation informations utilisateur et inventaire
    user_data = mdb.getInfos(session["id"])
    session["inventory"] = mdb.getProductByIdInventory(mdb.getCarsId(session["id"]), session["id"])
    users = []
    result = []

    if request.method == "POST":

        #### Recherche recursive d'utilisateurs ####
        try:
            search = request.form["search"]

            if search == '' or search == ' ':
                raise NameError
            conn, cur = mdb.connexion()
            search_object = mdb.RechercheUser(conn, cur)
            list_user = search_object.recherchePertinence("%" + search)
            if len(list_user) != 0:
                users = mdb.getUsersById(list_user[0])
                result = list_user[1]
            else:
                flash("No Result")
        #############################        

        except KeyError:

            try: # redirection vers les informations d'une voiture
                session["car_info"] = literal_eval(request.form["car"])
                return redirect(url_for('car'))

            except KeyError: # redirection vers la page user recherchee
                result = literal_eval(request.form["user"])
                session["user"] = result
                return redirect(url_for('user', user_name = result[2]))

        except NameError:
            pass

    if updateUserSession(user_data): # verification et mise a jour des cookies et des informations utilisateur
        session["nb_car"] = mdb.getNumberCar(session["id"])
        return render_template("profile.html", # affichage par defaut
                                id = session["id"],
                                name = session["name"], 
                                email = session["email"], 
                                new = session["new"],
                                money = session["money"],
                                age = session["age"],
                                nb_car = session["nb_car"],
                                date = session["date"],
                                inventory = session["inventory"],
                                users = users,
                                result = result,
                                drop = session["percent_drop"]) # renvoie a la page profile en cas de reussite




@app.route("/profile/edit", methods=["POST", "GET"]) # route vers l'edition du profil
def edit_profile():
    """
    permet d'editer le profil d'un utilisateur
    """
    if "email" not in session:
        return redirect(url_for('home'))
    
    if request.method == "POST":
        dicti = request.form
        email = session["email"]

        #### Photo de profil ####
        if 'pp' not in request.files: # verification de la photo
            flash('No file part detected')
            return redirect(url_for('edit_profile'))
        pp = request.files['pp']
        if pp.filename == '':
            flash('No profile picture selected')
            return redirect(url_for('edit_profile'))
        # ajout de la photo de profil dans le dossier
        if pp and allowed_file(pp.filename):
            filename = str(session["id"]) + file_extension(pp.filename)
            pp.save(os.path.join(app.config["UPLOAD_FOLDER"], filename))
        else: # test si l'image est bien en jpg
            flash('Your image must be in .jpg format')
            return redirect(url_for('edit_profile'))

        # redirection vers le profil apres les changements
        user_data = mdb.editUser(dicti, email)
        if updateUserSession(user_data):
            return redirect(url_for('profile'))

    
    return render_template("edit_profile.html", # renvoie la page edit_profile par defaut
                            name = session["name"], 
                            email = session["email"],
                            age = session["age"])

@app.route("/car", methods=["POST", "GET"]) # route vers car
def car():
    """
    permet d'afficher les informations d'une voiture et de l'acheter
    """
    if "email" not in session:
        return redirect(url_for('home'))

    if request.method == "POST":
        
        try: # annulation de la mise en vente d'une voiture aux autres utilisateurs
            cancel = literal_eval(request.form["cancel"])
            mdb.cancelSell(session["id"], cancel[0])


        except KeyError: # vente de la voiture
            car_info = literal_eval(request.form["car"])
            price = request.form["price"]
            if price != '': # mise en vente aux utilisateurs
                mdb.sellProductToUser(car_info, float(price), session["id"])
            else: # mise en vente au shop
                mdb.sellProduct(car_info, session["id"], session["percent_drop"])
                return redirect(url_for('profile'))
        

        return redirect(url_for('profile')) # renvoie vers le profil apres la transaction

    return render_template("car.html", # renvoie vers la page car par defaut
                            car_info = session["car_info"],
                            drop = session["percent_drop"])


@app.route("/profile/user/<user_name>", methods=["POST", "GET"]) # route vers le profil d'un utilisateur
def user(user_name):
    """
    permet d'afficher le profil d'un utilisateur et d'acheter ses voitures
    """
    if "email" not in session:
        return redirect(url_for('home'))
    if "user" not in session:
        return redirect(url_for('profile'))

    # acquisition des informations de l'utilisateur recherche
    user = session["user"]
    age = literal_eval(user[6])
    diff = datetime.now() - datetime(*age)
    session["age_user"] = diff.days
    session["nb_car_user"] = mdb.getNumberCar(user[0])
    session["inventory_user"] = mdb.getProductByIdInventory(mdb.getCarsId(user[0]), user[0])

# --------------------------------------------------#
# Voir pour différer les boutons 'BUY' et 'INFOS'   #
# qui font la même chose pour le moment,            #
# je crois que c là                                 #
# --------------------------------------------------#
    if request.method == "POST":
        # achat de voiture
        buy = literal_eval(request.form["car"])
        if (mdb.buyItemFromUser(session["id"], buy[0], user[0], buy[14])) : # test si la voiture est deja possedee et achat
            return redirect(url_for('profile')) # renvoie la page profile apres l'achat
        else:
            flash("You already have this car !")
            return render_template("user.html", # renvoie la page user si la voiture est deja en possession
                            user = user,
                            user_id = user[0],
                            age = session["age_user"],
                            nb_car = session["nb_car_user"],
                            inventory = session["inventory_user"],
                            new = user[4])

    return render_template("user.html", # renvoie de la page user par defaut
                            user = user,
                            user_id = user[0],
                            age = session["age_user"],
                            nb_car = session["nb_car_user"],
                            inventory = session["inventory_user"],
                            new = user[4])


@app.route("/shop", methods=["POST", "GET"]) # route vers shop
def shop():
    """
    permet d'afficher le magasin
    """
    if "email" not in session:
        return redirect(url_for('home'))
    
    # acquisition des voitures de la base de donnee
    products = mdb.getProducts()
    result = None
    
    if request.method == "POST":
        try:
            #### Recherche recursive de voitures ####
            search = request.form["search"]

            if search == '' or search == ' ':
                raise NameError

            conn, cur = mdb.connexion()
            search_object = mdb.RechercheCar(conn, cur)
            list_id = search_object.recherchePertinence("%" + search)
            if len(list_id) != 0:
                products = mdb.getProductById(list_id[0])
                result = list_id[1]
            else:
                flash("No Result")
            #############################    


        except KeyError: # achat de la voiture
            result = literal_eval(request.form["car"])
            session["car"] = result

            return redirect(url_for('buy_item', car_name = result[7]))


        except NameError:
            pass

    return render_template("shop.html", # renvoie de la page home par defaut
                            products = products,
                            result = result)

@app.route('/shop/buy/<car_name>', methods=["POST", "GET"]) # route vers l'achat d'une voiture
def buy_item(car_name):
    """
    permet d'acheter une voiture
    """
    if "email" not in session:
        return redirect(url_for('home'))
    if "car" not in session:
        return redirect(url_for('shop'))

    # acquisition des informations de la voiture a acheter
    result = session["car"]

    # achat de la voiture
    if request.method == "POST":
        session.pop("car")
        buy = request.form
        buy = literal_eval(request.form["item"])
        if mdb.buyProduct(buy, session["id"]): # achat
            return redirect(url_for('shop'))
        else:
            flash("You already have this car !") # renvoie si la voiture est deja en possession
        


    return render_template('confirm_buy.html', product = result) # renvoie la page confirm_buy par defaut

@app.route("/about") # route vers about
def about():
    """
    permet d'afficher la page about
    """
    return render_template("about.html") # renvoie simple de la page about


@app.route("/tree") # route vers tree
def tree():
    """
    permet l'execution, la creation d'arbres et l'affichage de la page trees
    """
    if "email" not in session:
        return redirect(url_for('home'))
    
    ## declaration des variables et creation (en boucle) de la photo tree.png ##
    a = Node(9)
    b = Node(8, parent=a)
    c = Node(7, parent=a)
    d = Node(6, parent=b)
    e = Node(5, parent=b)
    g = Node(3, parent=c)
    arbre.exportToPicture(a)
    ########

    # declaration des variables semblables a celles de la photo
    # elles serviront au code interne pour afficher les parcours d'arbres
    a = arbre.Arbre(9)
    b = arbre.Arbre(8)
    c = arbre.Arbre(7)
    d = arbre.Arbre(6)
    e = arbre.Arbre(5)
    g = arbre.Arbre(3)

    b.setLeft(d)
    b.setRight(e)
    c.setRight(g)
    a.setLeft(b)
    a.setRight(c)
    ####

    # creation de toutes les listes en meme temps
    # gain de temps
    list_parcours = []
    postfixe = arbre.postfixe(a, list_parcours)
    list_parcours = []
    infixe = arbre.infixe(a, list_parcours)
    list_parcours = []
    prefixe = arbre.prefixe(a, list_parcours)

    return render_template("trees.html", # renvoie la page trees avec toutes les listes et la photo mises a jour a chaque fois
                            postfixe = postfixe,
                            infixe = infixe,
                            prefixe = prefixe)

############################
##### Fin des routes #######
############################


## Manipulation des photos de profil ##
UPLOAD_FOLDER = 'static/profile_pictures' # declaration du dossier pour les photos de profil
ALLOWED_EXTENSIONS = ['jpg'] # seulement les photos en jpg sont acceptees
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

def allowed_file(filename):
    """
    verification de l'extension des photos de profl
    """
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS
def file_extension(filename):
    """
    recuperation de l'extension de la photo de profil
    """
    name, extension = os.path.splitext(filename)
    return extension


def updateUserSession(user_data) -> bool:
    """
    permet de mettre a jour les informations de l'utilisateur
    """
    try:
        session["id"] = user_data[0]
        session["email"] = user_data[1]
        session["name"] = user_data[2]
        session["new"] = user_data[4]
        session["money"] = user_data[5]
        return True

    except TypeError:
        return False

app.config['SECRET_KEY'] = "b'\xb9\xe71A.\xcc:\x87\x9dj\\j\xe8\x8d\x0eAN'"  # la clef secrete ne sert à rien pour l'instant
app.config['SESSION_TYPE'] = 'filesystem' # change ou confirme le type de session
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0 # permet de mettre a jour le cache du navigateur
# affiche les photos de profil, les images et les videos apres seulement une actualisation
date_created = datetime(2021, 3, 7, 10, 33, 55, 489611) # date de la creation de l'application

# Lancement de l'application
sess.init_app(app)
if __name__ == "__main__":
    app.run(debug = True)
