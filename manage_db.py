#### MANAGE DATABASE ####

# IMPORTS #
import sqlite3 as sql
from recherche import *
##########


def connexion():
    # Permet la connexion avec la BDD
    connexion = sql.connect("db/database.db")
    cursor = connexion.cursor()
    return (connexion, cursor)




##############################
######### TABLE USER #########
##############################

def addUser(dicti, date) -> bool:
    """
    ajoute un utilisateur lorsqu'il se connecte au site dans la table user
    renvoie True si l'utilisateur a ete ajoute
    renvoie False si l'utilisateur a entre un email existant deja dans la bdd
    :dicti: dictionnaire comportant ce que l'utilisateur a entre dans le <form>
    :date: string sous forme de tuple comportant la date de creation du compte
    """

    # Connexion avec la bdd
    conn, cur = connexion()

    # Implementation variables information utilisateur A REVOIR
    email = dicti['email']
    name = dicti['name']
    password = dicti['password']

    # verification de l'email
    email_check = cur.execute('SELECT * FROM user WHERE email=?;', (email,))
    if len(list(email_check)) != 0:
        return False
    
    # Inscription de l'utilisateur dans la bdd
    else:
        cur.execute('INSERT INTO user (email, name, password, new, money, age) VALUES (?,?,?,1,1000,?);', (email, name.lower(), password, date))
        conn.commit()
        conn.close()
        return True



def authUser(dicti) -> tuple:
    """
    verifie si les informations de connexion existent dans la bdd
    renvoie False si les informations ne correspondent pas
    renvoie la liste des informations si elles correspondent
    :dicti: dictionnaire comportant ce que l'utilisateur a entre dans le <form>
    """

    # Connexion avec la bdd
    conn, cur = connexion()

    # Implementation variables information utilisateur A REVOIR
    email = dicti['email']
    password = dicti['password']

    # Verification de l'email et du password
    email_pass_check = cur.execute('SELECT id, email, name, new, money, age, date FROM user WHERE email=? AND password=?;', (email, password))
    email_pass_check = list(email_pass_check)
    conn.close()
    if len(list(email_pass_check)) == 0:
        return False
    
    # Authentification de l'utilisateur
    else:
        return email_pass_check[0] # email_pass_check possede l'email et le nom de l'utilisateur



def editUser(dicti, email) -> tuple:
    """
    met a jour les informations du profil de l'utilisateur
    renvoie la nouvelle liste d'information utilisateur
    :dicti: dictionnaire comportant ce que l'utilisateur a entre dans le <form>
    :email: ancienne addresse email utilisateur pour le retrouver dans la bdd
    """

    # Connexion a la bdd
    conn, cur = connexion()

    # Implementation variables informations utilisateur A REVOIR
    new_email = dicti["email"]
    name = dicti["name"]

    # Mise a jour des informations utilisateur dans la bdd
    cur.execute('UPDATE user SET email=?, name=?, new=0 WHERE email=?;', (new_email, name, email))
    new_informations = list(cur.execute('SELECT id, email, name, new, money, age FROM user WHERE email=?;', (new_email,)))
    conn.commit()
    conn.close()
    return new_informations[0]



def getWallet(id, cur) -> tuple:
    """
    recupere la porte monnaie de l'utilisateur dans la BDD
    renvoie la valeur du porte monnaie de l'utilisateur
    :id: id de l'utilisateur
    :cur: curseur
    """
    wallet = list(cur.execute('SELECT money FROM user WHERE id=?;', (id, )))
    return wallet



def setWallet(money, user_id, cur, conn) -> bool:
    """
    met a jour la valeur du porte monnaie de l'utilisateur
    renvoie True si la mise a jour n'a pas eu d'erreur, False sinon
    :money: nouvelle valeur du porte monnaie
    :user_id: id de l'utilisateur
    :cur:, :conn: curseur
    """
    cur.execute('UPDATE user SET money=? WHERE id=?;', (money, user_id))
    conn.commit()
    return True



def getInfos(user_id) -> list:
    """
    recupere les informations de l'utilisateur
    renvoie la liste des informations
    :user_id: id de l'utilisateur
    """
    # Connexion a la BDD
    conn, cur = connexion()

    tuple_info = list(cur.execute('SELECT * FROM user WHERE id=?;', (user_id, )))[0]
    return tuple_info



def addDate(user_id, date) -> None:
    """
    met a jour la derniere date de connexion de l'utilisateur
    renvoie None
    :user_id: id de l'utilisateur
    :date: nouvelle date de derniere connexion
    """
    # Connexion a la BDD
    conn, cur = connexion()

    # Mise a jour de la date dans la BDD
    cur.execute('UPDATE user SET date=? WHERE id=?;', (date, user_id))
    conn.commit()
    conn.close()
    return None



def getCarsId(user_id) -> list:
    """
    recupere les informations de la voiture possedee par un utilisateur
    revoie la liste des informations
    :user_id: id de l'utilisateur
    """
    # Connexion a la BDD
    conn, cur = connexion()

    list_id = list(cur.execute('SELECT item_id, sell, price FROM link WHERE user_id=?;', (user_id, )))
    conn.close()
    return list_id



def getUsersById(user_id) -> list:
    """
    recupere les informations de utilisateurs dans :user_id:
    renvoie la liste des informations
    :user_id: est la liste des informations
    """
    # Connexion a la BDD
    conn, cur = connexion()
    # Chaque tupple d'information correspond a un item dans :user:
    user = []
    for i in user_id:
        user += list(cur.execute('SELECT * FROM user WHERE id=?;', (i,)))
    conn.close()
    return user




##############################
######## TABLE LINK ##########
##############################

def boughtProduct(user_id, item_id, cur, conn) -> None:
    """
    ajoute la voiture achetee par un utilisateur dans son inventaire
    renvoie None
    :user_id: id de l'utilisateur
    :item_id: id de la voiture
    :cur:, :conn: curseur
    """
    cur.execute('INSERT INTO link (user_id, item_id, sell, price) VALUES (?,?,0,0.0);', (user_id, item_id))
    conn.commit()
    return None



def soldProduct(user_id, item_id, cur, conn) -> None:
    """
    enleve la voiture vendue par un utilisateur de son inventaire
    renvoie None
    :user_id: id de l'utilisateur
    :item_id: id de la voiture
    :cur:, :conn: curseur
    """
    cur.execute('DELETE FROM link WHERE user_id=? AND item_id=?;', (user_id, item_id))
    conn.commit()
    return None


def checkAlreadyGot(user_id, item_id, cur, conn) -> bool:
    """
    verifie si un utilisateur (:user_id:) possede deja une voiture (:item_id:) dans son inventaire
    renvoie True s'il la possede, False sinon
    :user_id: id de l'utilisateur
    :item_id: id de la voiture
    :cur:, :conn: curseur 
    """
    if len(list(cur.execute('SELECT * FROM link WHERE user_id=? AND item_id=?;', (user_id, item_id)))) == 0:
        return True
    return False



def getNumberCar(user_id) -> int:
    """
    recupere le nombre de voiture que possede un utilisateur 
    renvoie le nombre de voitures
    :user_id: id de l'utilisateur
    """
    # Connexion a la BDD
    conn, cur = connexion()

    list_car = list(cur.execute('SELECT * FROM link WHERE user_id=?;', (user_id, )))
    conn.close()
    return len(list_car)



def sellProductToUser(tuple, price, user_id) -> None:
    """
    met en vente une voiture pour les utilisateurs
    renvoie None
    :tuple: informations de la voiture
    :price: prix de vente de la voiture
    :user_id: id de l'utilisateur
    """
    # Connexion a la BDD
    conn, cur = connexion()

    cur.execute('UPDATE link SET sell=1, price=? WHERE user_id=? AND item_id=?;', (price, user_id, tuple[0]))
    conn.commit()
    conn.close()
    return None



def cancelSell(user_id, item_id) -> None:
    """
    annule la mise en vente d'une voiture aux autres utilisateur
    renvoie None
    """
    # Connexion a la BDD
    conn, cur = connexion()

    cur.execute('UPDATE link SET sell=0, price=0 WHERE user_id=? AND item_id=?;', (user_id, item_id))
    conn.commit()
    conn.close()
    return None



def buyItemFromUser(user_id, item_id, user_sell_id, price) -> bool:
    """
    permet d'acheter une voiture mise en vente pour les autres utilisateurs
    renvoie True si l'achat a eu lieu, False sinon
    :user_id: id de l'utilisateur
    :item_id: id de la voiture
    :user_sell_id: id de l'utilisateur vendant la voiture
    :price: prix de vente de la voiture
    """
    # Connexion a la BDD
    conn, cur = connexion()

    if checkAlreadyGot(user_id, item_id, cur, conn): # verification si la voiture est deja en possesion de l'utilisateur
        # cote acheteur
        boughtProduct(user_id, item_id, cur, conn) # achat de la voiture
        wallet = getWallet(user_id, cur) # recuperation du porte monnaie
        result = wallet[0][0] - price 
        setWallet(result, user_id, cur, conn) # mise a jour du porte monnaie

        # cote vendeur
        soldProduct(user_sell_id, item_id, cur, conn) # achat de la voiture
        wallet = getWallet(user_sell_id, cur) # recuperation du porte monnaie
        result = wallet[0][0] + price
        setWallet(result, user_sell_id, cur, conn) # mise a jour du porte monnaie
        
        conn.close()
        return True

    return False






##############################
######### TABLE ITEM ########
##############################

def getProducts() -> list:
    """
    recupere la liste des voitures dans la BDD
    renvoie la liste
    """
    # Connexion a la BDD
    conn, cur = connexion()

    products_list = list(cur.execute('SELECT * FROM item;'))
    conn.close()
    return products_list



def getProductByIdInventory(item_id, user_id) -> list:
    """
    recupere les informations des voitures de l'inventaire d'un utilisateur
    renvoie la liste des informations
    :item_id: ids des voitures
    :user_id: id de l'utilisateur
    """
    # Connexion a la BDD
    conn, cur = connexion()

    product = []
    for i in item_id:
        product += list(cur.execute('SELECT * FROM item JOIN link ON item.id = link.item_id WHERE id=? AND link.user_id=?;', (i[0], user_id)))
    conn.close()
    return product



def getProductById(item_id) -> list:
    """
    recupere les informations d'une voiture dans la BDD en fonction de son id
    renvoie la liste des informations
    :item_id: id de la voiture
    """
    # Connexion a la BDD
    conn, cur = connexion()

    product = []
    for i in item_id:
        product += list(cur.execute('SELECT * FROM item WHERE id=?', (i,)))
    conn.close()
    return product



def buyProduct(tuple, user_id) -> bool:
    """
    procedure achetant une voiture
    renvoie True s'il n'y a pas eu d'erreurs, False sinon
    :tuple: la liste d'informations de la voiture
    :user_id: id de l'utilisateur
    """
    # Connexion a la BDD
    conn, cur = connexion()

    # calcul du nouveau montant du porte monnaie
    wallet = getWallet(user_id, cur)
    result = wallet[0][0] - tuple[-2]

    if checkAlreadyGot(user_id, tuple[0], cur, conn): # verification si la voiture est deja possedee
        if setWallet(result, user_id, cur, conn): # mise a jour du porte monnaie
            boughtProduct(user_id, tuple[0], cur, conn) # achat de la voiture

            conn.close()
            return True
    return False

def sellProduct(tuple, user_id, drop) -> None:
    """
    procedure vendant une voiture
    renvoie None
    :tuple: la liste d'informations de la voiture
    :user_id: id de l'utilisateur
    :drop: pourcentage de reduction de la valeur de la voiture
    """
    # Connexion a la BDD
    conn, cur = connexion()

    # calcul du nouveau montant du porte monnaie
    wallet = getWallet(user_id, cur)
    result = wallet[0][0] + tuple[9]*drop

    if setWallet(result, user_id, cur, conn): # mise a jour du porte monnaie
        soldProduct(user_id, tuple[0], cur, conn) # vente de la voiture

        conn.close()
    return None