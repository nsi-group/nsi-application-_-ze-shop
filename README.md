# Application Ze Shop
### Par PapyRuss (Victor) et Ender_Night (Mathéo)
## Sommaire :
1. Presentation de l'application
2. Installation
3. Règles, conditions et principes d'utilisation
4. Arborescence du dépôt

## 1. Presentation de l'application

Bienvenue sur l'application **_Ze Shop_** !!  
  
Ici, vous pourrez **acheter** les plus belles voitures dont vous avez toujours révé, **ravaliser** avec vos amis, **collectionner** le plus de voitures ou bien même **développer** votre propre marché !  
  
Notre application se base sur l'achat, la vente et la collection de voitures en tout genres :
 * Vous avez la possibilité de **créer** un compte, vous permettant d'acheter vos premières voitures au magasin, puis de les **revendre** à d'autres utilisateurs ou bien directement à la source, mais avec une petite réduction quand même !

 * Vous avez aussi la possibilité d'**acheter** de nombreuses voitures au magasin, grâce à un catalogue bien fourni.
  
 * Vous avez aussi la possibilité de **collectionner** vos voitures et de les voir dans votre propre **inventaire**. Chaque voiture a ses **propres caractéristiques**, uniques ou non. Vous avez la possibilité de voir toutes ces dernières au magasin ou bien directement sur la page dédiée aux informations de la voiture. De plus, libre à vous de **revendre** vos voitures ! Vous avez le choix entre la revente au magasin ou bien au monde entier en fixant votre propre prix !

 * Vous avez ensuite la possibilité de **rendre visite** aux autres utilisateurs, à vos meilleurs amis, à vos acheteurs, vos vendeurs, bref, au monde entier. Vous pourrez **comparer** leurs inventaires de voitures au votre, **constater** de la mise en vente d'une de leur voiture, **l'acheter**, et ainsi de suite. Cela donne de très nombreuses possibilités quant à la **création de votre propre marché** !

 * Nous avons aussi ajouté des **fonctions de recherche pertinente**, vous permettant d'atteindre vos résultats, même avec des fautes de frappes ;).

 * Vous pouvez aussi **accéder** à une page spéciale vous apprenant les différents parcours d'arbre binaire. De quoi bien vous familiariser avec tout ça !

 * Enfin, si l'envie vous prend, nous avons une page **about** où nous nous présentons !

## 2. Installation et lancement

Pour installer l'application, il est nécessaire de [télécharger](https://gitlab.com/nsi-group/nsi-application-_-ze-shop) le dépôt.  
* Pour Windows  
 Installez ou mettez a jour les modules présents dans [requirements.txt](https://gitlab.com/nsi-group/nsi-application-_-ze-shop/-/blob/feature_internal_server/requirements.txt).  
 Lancez une console depuis votre dossier et tapez la commande :  
 ```bash
 pip install -r requirements.txt 
 ```
  Il vous faut ensuite installer l'application **Graphviz**. Rendez-vous sur ce [site](https://graphviz.org/download/#windows) puis installez **Graphviz** sur votre ordinateur en suivant les procédures indiquées.

 * Pour linux  
 Même schéma que pour windows, lancez un terminal depuis votre dossier et tapez la commande pour installer les dépendances :  
 ```bash
 pip install -r requirements.txt 
 ```
 Il vous faut ensuite installer l'application **Graphviz**. Tapez cette commande dans le même terminal après avoir installé les dépendances (adaptez cette commande en fonction de votre distribution) :
 ```bash
sudo apt install graphviz
 ```

 * Pour macOs  
 Encore une fois le même schéma que pour les deux autres systèmes au dessus, lancez un terminal depuis votre dossier et tapez la commande pour installer les dépendances :  
 ```bash
 pip install -r requirements.txt 
 ```
 Il vous faut ensuite installer l'application **Graphviz**. Tapez cette commande dans le même terminal après avoir installé les dépendances :
 ```bash
brew install graphviz
 ```
Ensuite, il faut lancer le fichier python [main.py](https://gitlab.com/nsi-group/nsi-application-_-ze-shop/-/blob/feature_internal_server/main.py). Copiez par la suite le lien affiché depuis votre console ou votre terminal dans votre navigateur favori et la page d'accueil devrait apparaître.

## 3. Règles, conditions et principes d'utilisation
Voici une description détaillée de toutes les pages de l'application ainsi que toutes les fonctions associées.

### 1. **Page d'accueil**  
La page d'accueil est la page s'affichant par défaut en lançant l'application. Elle possède deux versions :
* Version invité
 ![Page d'accueil en étant déconnecté](readme_img/home_logout.png)
 La page est composée de plusieurs zones :
  * La barre de navigation
    * Le bouton Home permet de revenir à la page d'accueil.
    * Le bouton About permet d'afficher la page about, possédant les informations à propos des développeurs.
    * Le bouton Login permet à un utilisateur possédant un compte de se connecté à se dernier.
    * Le bouton Sign Up permet à un utilisateur ne possédant pas de compte d'en créer un.
  * L'encart d'information sur la manière d'utilisation du site
  * Une zone possédant les deux noms des développeurs ainsi qu'un lien vers la page about

* Version connecté
 ![Page d'accueil en étant connecté](readme_img/home_login.png)
 La version connectée n'est accessible qu'aux utilisateurs connectés. Elle possède des zones différentes :
  * La barre de navigation :
    * Le bouton Shop affiche le magasin
    * Le bouton Profile affiche le profil de l'utilisateur connecté
    * Le bouton logout permet de déconnecté l'utilisateur connecté
   
### 2. **Page Sign Up**
La page Sign Up est la page s'affichant lorsqu'un utilisateur souhaite s'inscrire dans l'application, après qu'il ait appuyé sur le bouton Sign Up dans la page d'accueil.
![Page Sign Up](readme_img/signup.png)
Il faut remplir les inputs avec un email, un nom et un mot de passe puis cliquer sur le bouton Sing Up en vert. Il y a possibilité de revenir à la page d'accueil en cliquant sur le bouton Cancel en rouge.  
Dans le cas où l'email entré est déjà utilisé par un autre utilisateur, un message d'erreur apparaitera :
![Page Sing Up avec erreur](readme_img/signup_error.png)
L'utilisateur est poussé à entrer un autre email ou de se connecter si l'email est le sien.  
Par la suite, l'utilisateur est renvoyé vers sa propre page profil fraichement créée.

### 3. **Page Login**
La page Login est la page s'affichant lorsqu'un utilisateur souhaite s'inscrire dans l'application, après qu'il ait appuyé sur le bouton Login dans la page d'accueil.
![Page Login](readme_img/login.png)
Il faut remplir les inputs avec son propre email ainsi que son mot de passe. De même que dans la page Sign Up, il faut appuyé sur le bouton vert Login pour se connecter ou bien sur le bouton rouge Cancel pour revenir à la page d'accueil.  
Dans le cas ou le mot de passe entré ne correspond pas au bon mot de passe, un message d'erreur apparaiera :
![Page Login avec erreur](readme_img/login_error.png)
L'utilisateur est poussé à retenter de se connecter ou bien de créer un compte.
Par la suite, l'utilisateur est renvoyé vers sa propre page profil.

### 4. **Page Profile**
La page Profile est la page s'affichant lorsqu'un utilisateur se connecte ou bien appuie sur un bouton Profile sur une des pages de l'application. Cette page ne s'affiche que lorsqu'un utilisateur est connecté et est unique à celui-ci.
![Page Profile Haut](readme_img/profile_up.png)
La page est composée de plusieurs zones :
 * La barre de navigation :
   * Le bouton Logout permet de déconnecter l'utilisateur connecté.
   * Le bouton Return Home permet de revenir à la page d'accueil.
 * L'encart Informations  
 Cet encart possède toutes les informations de l'utilisateur connecté (email, nom, age depuis la création du compte, porte-monnaie, nombre de voitures, dernière date de connexion).
 * Le lien vers l'édition du profil  
 Ce lien permet à l'utilisateur de modifier les informatinos dans l'encart Informations, en plus de sa photo de profil.
 
![Page Profile Bas](readme_img/profile_down.png)
 * L'encart Inventaire  
 Cet encart affiche la liste des voitures en la possession de l'utilisateur, ainsi qu'un lien vers les pages d'informations dédiées à ces voitures. C'est dans ces pages que la voiture peut être vendue.
 * L'encart Users  
 Cet encart affiache la liste des utilisateurs trouvés lors de la recherche récursive et affiche quelques informations ainsi qu'un lien spécifique vers leur page profil public.
 * L'encart Parcours d'arbre  
 Cet encart redirige vers la page accueillant les parcours d'arbres


### 5. **Page Infos**
Ces pages sont spécifiques à chaque voitures possédées par l'utilisateur. Elles présentent quelques informations sur la voiture, la possibilité de la revendre au magasin avec une réduction ou bien de choisir de revendre la voiture au monde entier.  
![Page Infos](readme_img/car_infos.png)  
Les informations de la voiture sont très sommaires puisqu'on y retrouve uniquement le nom du model. À contrario, le prix occupe un eplace importante puisqu'il correspond au prix d'achat de la voiture.  
* L'encart de la vente  
 Cet encart permet de vendre par defaut la voiture au magasin au prix indiqué sur le bouton. Ce prix correspond au prix d'achat de la voiture moins le pourcentage de réduction quotidien.  
 Il y a aussi la possibilité de revendre la voiture au monde entier en cochant la case "Sell for users". Il est alors demandé le prix de vente puis de confirmer la vente. **Il est toujours possible d'annuler la vente en retournant sur la page info de la voiture et de cliquer sur le bouton "Cancel the sell".**

### 6. **Page User**
Ces pages sont spécifiques à chaque utilisateur de l'application. Elles affichent le profil public de chaque utilisateur.
![Page User](readme_img/user.png)
Cette page possède la même barre de navigation que la page Profile.
La page est autrement composée de plusieurs zones :
 * L'encart Informations utilisateur  
 Possède toutes les informations publiques de l'utilisateur (semblables à celles de la page profile) ainsi que la photo de profil de l'utilisateur.
 * L'encart inventaire  
 Cet encart affiche la liste de toutes les voitures que l'utilisateur possède. Si l'utilisateur à mis en vente une voiture au monde entier, alors un bouton pour acheter la voiture apparaîtra. Si la voiture est déjà en pessions du visiteur, en message d'erreur sera affiché, sinon le visiteur sera redirigé vers sa page profil.

### 7. **Page Parcours d'Arbre**
Cette page affiche un arbre et plusieurs choix de parcours d'arbre binaire. Lorsqu'un utilisateur coche une case, le parcours correspondant est affiché en dessous de l'arbre et le reste des choix est supprimé. Il suffit de décocher  la case pour refaire apparaître les autres cases et faire disparaître le parcours. À vous de tester !
![Page Parcours d'Arbre](readme_img/parcours.png)

### 8. **Page Magasin**
Cette page concentre le magasin et toutes ses fonctions. C'est ici que l'on peut acheter toutes les voitures dont vous avez rêvé ! Un utilisateur verra par defaut toute la liste des voitures et devra passer sa souris par dessus une d'entre elle pour pouvoir l'acheter. Dans le cas ou l'utilisateur recherche une voiture, il peut entrer le nom de celle-ci dans la barre de recherche. Dans ce cas, uniquement les voitures correspondant à la recherche seront affichées.
![Page Shop](readme_img/shop.png)

### 9. **Page Informations**
Cette page est semblable à la page informations accessible via la page profil. Cette page permet d'afficher les informations détaillées d'une voiture spécifique ainsi que de l'achater lorsqu'un utilisateur clique sur "BUY" sur une carte de voiture. C'est un achat au magasin, la voiture ne pourra pas être revendue à ce dernier plus chère qu'elle n'a été achetée.
![Page Infos](readme_img/car_infos_shop.png)

## 4. Arborescence du dépôt
Le dépôt est construit comme suit :  
![Arborescence](readme_img/repo_structure.png)  
* Les principaux fichiers python sont situés à la racine du dépot.
* La base de données est située dans le dossier db.
* Tous les fichiers statiques (images, videos, css, js) sont situés dans le dossier static.
* Toutes les pages html sont dans le dossier templates.  


**Merci d'avoir téléchargé notre projet ! Nous espérons que vous vous amusez comme des fous à collectionner, vendre et échager les voitures de vos rêves !**

Par PapyRuss (Victor) et Ender Night (Mathéo), le 27 avril 2021 (date de création du projet à la fin du fichier [main.py](https://gitlab.com/nsi-group/nsi-application-_-ze-shop/-/blob/feature_internal_server/main.py))