#### RECHERCHE ####

class RechercheCar:
    """
    permet la recherche avancee d'une voiture dans la BDD
    """

    # constructeur
    def __init__(self, conn, cur):
        # curseur
        self.conn = conn
        self.cur = cur
    
    ## getter et setter
    def getConn(self):
        return self.conn

    def getCur(self):
        return self.cur

    def setConn(self, conn):
        self.conn = conn

    def setCur(self, cur):
        self.cur = cur
    ####

    def rechercheMot(self, mot:"str") -> list:
        """
        recherche les informations d'une voiture pour un mot lettres pour lettres
        renvoie les informations de la voiture
        :mot: mot recherche
        """
        mot = mot.lower()
        list_models = list(self.cur.execute('SELECT id FROM item WHERE model=?;', (mot,)))

        return list_models[0][0]

    def recherchePertinence(self, mot:"str"):
        """
        recherche les informations d'une voiture pour un mot par pertinance et recursivement
        renvoie les informations de la voiture
        :mot: mot recherche
        """
        if mot != "%":
            mot = mot.lower() + "%"
            list_models = list(self.cur.execute('SELECT id FROM item WHERE model LIKE ?;', (mot, )))
            if len(list_models) == 0:
                return self.recherchePertinence(mot[0:-2])
            else:
                return ([item for i in list_models for item in i], mot.replace("%", ''))
        else:
            return []


class RechercheUser:
    """
    permet la recherche avancee d'une voiture dans la BDD
    """
    # constructeur
    def __init__(self, conn, cur):
        # curseur
        self.conn = conn
        self.cur = cur

    ## getter et setter
    def getConn(self):
        return self.conn

    def getCur(self):
        return self.cur

    def setConn(self, conn):
        self.conn = conn

    def setCur(self, cur):
        self.cur = cur
    ####


    def rechercheMot(self, mot:"str"):
        """
        recherche les informations d'un utilisateur pour un mot lettres pour lettres
        renvoie les informations de l'utilisateur
        :mot: mot recherche
        """
        mot = mot.lower()
        list_users = list(self.cur.execute('SELECT id FROM user WHERE name=?;', (mot, )))
        if len(list_users) != 0:
            return [list_users[0][0]]
        return []

    def recherchePertinence(self, mot:"str"):
        """
        recherche les informations d'un utilisateur pour un mot par pertinance et recursivement
        renvoie les informations de l'utilisateur
        :mot: mot recherche
        """
        if mot != "%":
            mot = mot.lower() + "%"
            list_users = list(self.cur.execute('SELECT id FROM user WHERE name LIKE ?;', (mot, )))
            if len(list_users) == 0:
                return self.recherchePertinence(mot[0:-2])
            else:
                return ([item for i in list_users for item in i], mot.replace("%", ''))
        else:
            return []
